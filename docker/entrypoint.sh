#!/usr/bin/env bash

if [ -z "$WORKDIR" ]; then
  WORKDIR=$(cd $(dirname $0) && pwd)
fi

if [ -z $SKIP_MERGE_ENV_FILES ]; then
  ENV_FILES=$(ls $WORKDIR/.env.*)
  BASE_ENV_FILE="$WORKDIR/.env"

  echo -n "" > "$BASE_ENV_FILE"

  # merging env files
  echo "Merging .env files"
  for env_file in $ENV_FILES; do
    echo "- $env_file"
    cat "$env_file" >> "$BASE_ENV_FILE"
  done
fi

python Syncer.py $@