#!/usr/bin/env bash

WORKDIR=$(cd $(dirname $0) && pwd)
ENV_FILES=$(ls $WORKDIR/.env.*)
BASE_ENV_FILE="$WORKDIR/.env"

echo -n "" > "$BASE_ENV_FILE"

# merge .env files
echo "Merging .env files"
for env_file in $ENV_FILES; do
  echo " - "$(basename $env_file)
  cat "$env_file" >> "$BASE_ENV_FILE"
done
echo

docker-compose -f docker-compose.yml -f docker-compose.dev.yml run -e SKIP_MERGE_ENV_FILES=true syncer