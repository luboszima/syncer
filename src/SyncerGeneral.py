import json

from connectors import BaseSync
from exceptions import InstanceException

# TODO popis tridy
# TODO popis metod
class Syncer:
    """
        Get sync between connectors

        Parameters
        ----------
        start_date : Datetime, required
            Since when get the entries (default is None)
        end_date : Datetime, required
            Until when to take the entries (default is None)

        Raises
        ------
        InstanceException
            If no instance of connector exist.
        """

    start_date = ""
    end_date = ""

    from_instance = False
    to_instance = False

    time_chunk = False
    time_chunk_roundup = False

    verbose = False
    debug = False

    def __init__(self, from_instance,
                 to_instance,
                 start_date,
                 end_date,
                 time_chunk,
                 time_chunk_roundup,
                 default_timezone,
                 verbose=False,
                 debug=False,
                 ):

        # check instances
        if not isinstance(from_instance, BaseSync):
            raise InstanceException("from_instance is not instance of BaseSyncer")

        if not isinstance(to_instance, BaseSync):
            raise InstanceException("to_instance is not instance of BaseSyncer")

        # set variables
        self.from_instance = from_instance
        self.to_instance = to_instance
        self.start_date = start_date
        self.end_date = end_date

        self.time_chunk = time_chunk
        self.time_chunk_roundup = time_chunk_roundup

        self.debug = debug
        self.verbose = verbose

    def sync(self):
        print("SYNC ALL ENTRIES")
        entries = self.from_instance.get_entries(self.start_date, self.end_date)
        self.to_instance.delete_entries(self.start_date, self.end_date)
        self.to_instance.set_entries(entries)
        print("SYNC DONE.")