TogglPy==0.1.2
requests==2.26.0
jira==3.1.1
python-dateutil==2.8.2
tempo-api-python-client==0.5.0