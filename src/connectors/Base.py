import json, re
from datetime import timedelta
import exceptions


class BaseSync:
    """
        Base class for connectors.

        Attributes
        ----------
        username : str
            username needed to log in to the service, for login use username + password or just api token
        password : str
            password needed to log in to the service, for login use username + password or just api token
        token : str
            api token needed to log in to the service, for login use username + password or just api token
        debug : bool
            When debug mode is enabled, details of actions are printed, no actions are written to the selected application

        Methods
        -------
        login()
            Log in to connector app.
        get_entries()
            get time entries from connector app
        get_entries()
            get time entries from connector app
        """

    username = ''
    password = ''
    token = ''
    config = None

    debug = False
    verbose = False
    default_timezone = False
    time_chunk = False
    time_chunk_roundup = False

    UNITS = {"s": "seconds", "m": "minutes", "h": "hours", "d": "days", "w": "weeks"}

    def __init__(self, username=None,
                 password=None,
                 token=None,
                 debug=False,
                 verbose=False,
                 config=None,
                 time_chunk=None,
                 time_chunk_roundup=None,
                 default_timezone="Europe/Prague"
                 ):
        self.username = username
        self.password = password
        self.token = token
        self.debug = debug
        self.verbose = verbose
        self.config = json.dumps(config)
        self.config = json.loads(self.config)
        self.default_timezone = default_timezone
        self.time_chunk = time_chunk
        self.time_chunk_roundup = time_chunk_roundup

        self.login()


    def set_config_identificators(self):
        identificators = {}

        if "identificator" in self.config:
            for identificator in self.config["identificator"]:
                parsed_object = self.parse_config_identificator(identificator)
                identificators[identificator] = parsed_object

        return identificators

    def login(self):
        print("Jira login")
        raise NotImplementedError("Method \"login\" isn't implemented in child class.")

    def get_entries(self, start_date=None, end_date=None):
        """
        Get entries

        Gets the entries in the connector and returns them in the correct format

        Return data:
        [
            {
                "datetime_from": "<datetime>",
                "datetime_to": "<datetime>",
                "description": "<string>",
                "identificator": "<project_id>",
            }
        ]

        Parameters
        ----------
        start_date : str, required
            Since when get the entries (default is None)
        end_date : str, required
            Until when to take the entries (default is None)

        Raises
        ------
        NotImplementedError
            If no sound is set for the animal or passed in as a
            parameter.
        """
        print("Get Entries")

        if not start_date:
            raise NotImplementedError("Parameter 'start_date' required.")

        if not start_date:
            raise NotImplementedError("Parameter 'end_dat' required.")

    def set_entries(self, entries=None):
        print("Set Entries")
        raise NotImplementedError("Method \"set_entries\" isn't implemented in child class.")

    def delete_entries(self, start_date=None, end_date=None):
        print("Delete Entries")
        if not start_date:
            raise NotImplementedError("Parameter 'start_date' required.")

        if not start_date:
            raise NotImplementedError("Parameter 'end_dat' required.")

    def test(self, entries=None):
        print("Test Entries")

        if not entries:
            raise NotImplementedError("Parameter 'entries' required.")

    def convert_to_seconds(self, s):
        count = int(s[:-1])
        unit = self.UNITS[s[-1]]
        td = timedelta(**{unit: count})
        return td.seconds + 60 * 60 * 24 * td.days

    def roundup_time_in_seconds(self, seconds):
        time_chunk_roundup_seconds = self.convert_to_seconds(self.time_chunk_roundup)
        time_chunk_seconds = self.convert_to_seconds(self.time_chunk)
        rest_after_division = seconds % time_chunk_seconds
        divisiton_without_rest = int(seconds / time_chunk_seconds)
        result = divisiton_without_rest * time_chunk_seconds

        if rest_after_division > time_chunk_roundup_seconds:
            result += time_chunk_seconds

        return result

    def modify_entries(self, entries):
        """
        Modify entries

        Modify list of entries by rules from json config (TO/FROM_CONFIG)

        Return data:
        [
            {
                "datetime_from": "<datetime>",
                "datetime_to": "<datetime>",
                "description": "<string>",
                "identificator": "<project_id>",
            }
        ]

        Parameters
        ----------
        entries : list, required
            entries from self.get_entries()
        """
        entries_tmp = entries

        if 'entries' in self.config and self.config['entries'] == 'group':
            entries_tmp = self.group_entries_by_description(entries_tmp)

        spread_in_seconds = 0
        if 'identificator' in self.config:
            for identificator in self.config['identificator']:

                parsed_object = self.parse_config_identificator(identificator)
                name = parsed_object['name']
                spread = parsed_object['spread']

                # skip some entries
                if name == 'skip':
                    for index, entry in enumerate(entries_tmp):
                        if re.match(entry['identificator'], name):
                            entries_tmp.pop(index)

                # spread time from entry to rest of the entries
                elif name == 'spread':
                    for index, entry in enumerate(entries_tmp):
                        if re.match(entry['identificator'], name):
                            # spread time only to entries where is it allowed
                            spread_in_seconds += self.get_time_spent(entry)
                            entries_tmp.pop(index)

            # spread values to rest of entries
            if spread_in_seconds > 0:
                entries_tmp = self.spread_time_to_entries(entries_tmp, spread_in_seconds)

            # replace identificators have to be after time spread
            for identificator in self.config['identificator']:

                parsed_object = self.parse_config_identificator(identificator)
                name = parsed_object['name']

                # replace identificator for issue
                for index, entry in enumerate(entries_tmp):
                     if entry['identificator'] == identificator:
                         config_identificator = self.get_config_identificator_name(self.config['identificator'][identificator])
                         entries_tmp[index]['identificator'] = config_identificator

        return entries_tmp

    def get_config_identificator_name(self, identificator):
        name = ""
        if isinstance(identificator, str):
            name = identificator
        elif isinstance(identificator, dict):
            if not "name" in identificator:
                # throw exception if name is not set
                raise ParamException(
                    f"Config for connector \"${type(self).__name__}\" contains identifier \"${identificator}\" with extended definition but without required name field.")
            name = identificator['name']

        return name


    def parse_config_identificator(self, identificator):
        spread = True
        name = ""

        value = self.config['identificator'][identificator]
        if isinstance(value, str):
            name = value
        elif isinstance(value, dict):
            if "name" in value:
                name = value['name']
            else:
                # throw exception if name is not set
                raise ParamException(
                    f"Config for connector \"${type(self).__name__}\" contains identifier \"${identificator}\" with extended definition but without required name field.")

            if "spread" in value:
                spread = value['spread']

        return {
            "name": name,
            "spread":  spread
        }

    def spread_time_to_entries(self, entries, spread_seconds):

        # get time entries where i can spread time
        tmp_entries_spread = []
        tmp_entries_rest = []
        for index, entry in enumerate(entries):
            if (not entry['identificator'] in self.config_identificators) or (self.config_identificators[entry['identificator']]['spread']):
                tmp_entries_spread.append(entry)
            else:
                tmp_entries_rest.append(entry)

        count_entries = len(tmp_entries_spread)
        for_every_entry_seconds = int(spread_seconds / count_entries)

        # spread time to entries
        for index, entry in enumerate(tmp_entries_spread):
            spent_time_seconds = self.get_time_spent(entry)
            spent_time_seconds += for_every_entry_seconds
            tmp_entries_spread[index]['datetime_to'] = entry['datetime_from'] + timedelta(seconds=spent_time_seconds)

        results = tmp_entries_spread + tmp_entries_rest

        return results


    def group_entries_by_description(self, entries):
        tmp_array = {}
        new_entries = []

        # group by description
        for entry in entries:
            # grouped by description, identificator and current day
            key = entry['description']+entry['identificator']+entry['datetime_from'].strftime("%Y-%m-%d")
            if key not in tmp_array:
                tmp_array[key] = []

            tmp_array[key].append(entry)

        # addition same entries to just one
        for key in tmp_array:
            tmp_entry = self.entry_addition(tmp_array[key])
            new_entries.append(tmp_entry)

        return new_entries

    def get_time_spent(self, entry):
        return (entry['datetime_to'] - entry['datetime_from']).total_seconds()

    def entry_addition(self, entries):

        tmp_entry = {'datetime_from': None, 'datetime_to': None, 'description': None, 'identificator': None}

        # for entry in entries:
        spent_time_seconds = 0
        for index, entry in enumerate(entries):
            if index == 0:
                tmp_entry['description'] = entry['description']
                tmp_entry['identificator'] = entry['identificator']
                tmp_entry['datetime_from'] = entry['datetime_from']

            spent_time_seconds += self.get_time_spent(entry)

        tmp_entry['datetime_to'] = tmp_entry['datetime_from'] + timedelta(seconds=spent_time_seconds)

        return tmp_entry

    def skip_set_entry(self, identificator):
        if 'identificator' in self.config:
            for issue in self.config['identificator']:
                if re.match(issue, identificator) and self.config['identificator'][issue] == "skip-set":
                    return True
        return False

    def skip_delete_entry(self, identificator):
        if 'identificator' in self.config:
            for issue in self.config['identificator']:
                if re.match(issue, identificator) and self.config['identificator'][issue] == "skip-delete":
                    return True
        return False