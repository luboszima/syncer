import json
import os
import sys
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
import datetime
from jira import JIRA, JIRAError
from tempoapiclient import client
import re

from .Base import BaseSync

# TODO predavat konfig pro pripojeni k ruznym projektum
# -- nejspis bude potreba i vice tokenu nebo loginu pro jine projekty
# -- overit ze jeden token k jire staci i na pristup k treba Drmax ukolum
from exceptions import InstanceException


class JiraSync(BaseSync):
    jira = False
    tempo = False

    def __init__(self,
                 username,
                 password,
                 token,
                 config,
                 debug,
                 verbose,
                 default_timezone,
                 time_chunk,
                 time_chunk_roundup
                 ):
        super(JiraSync, self).__init__(username=username,
                                       password=password,
                                       token=token,
                                       config=config,
                                       default_timezone=default_timezone,
                                       time_chunk=time_chunk,
                                       time_chunk_roundup=time_chunk_roundup,
                                       debug=debug,
                                       verbose=verbose
                                       )

        # init values
        self.config = json.loads(self.config)

    def login(self):
        try:
            config = json.loads(self.config)
            jira_url = config["jira_url"]
            tempo_token = config["tempo_token"]

            self.jira = JIRA(jira_url, basic_auth=(self.username, self.token))
            self.tempo = client.Tempo(
                auth_token=tempo_token,
                base_url="https://api.tempo.io/core/3"
            )

        except JIRAError:
            if not self.jira:
                raise InstanceException(type(self).__name__ + " bad login.")

    def get_entries(self, start_date="", end_date=""):
        """
        Get entries

        Gets the entries in the connector and returns them in the correct format

        Return data:
        [
            {
                "datetime_from": "<datetime>",
                "datetime_to": "<datetime>",
                "description": "<string>",
                "identificator": "<project_id>",
            }
        ]

        Parameters
        ----------
        start_date : Datetime, required
            Since when get the entries (default is None)
        end_date : Datetime, required
            Until when to take the entries (default is None)

        Raises
        ------
        NotImplementedError
            If no sound is set for the animal or passed in as a
            parameter.
        """

        # debug output
        if self.verbose:
            print("Get Entries Jira")

        start_date = parse(start_date)
        dt_end_date = parse(end_date)

        worklog_date_start = start_date.strftime("%Y-%m-%d")
        worklog_date_end = dt_end_date.strftime("%Y-%m-%d")

        # debug output
        if self.verbose:
            print("Find issues from date ", worklog_date_start, " to date ", worklog_date_end)

        entries = []
        # get all issues I worked on, between start and end dates
        issues = self.jira.search_issues(
            f'(worklogDate > "{worklog_date_start}" AND worklogDate < "{worklog_date_end}") AND ( assignee=currentUser() OR status changed TO "In Progress" BY currentUser() OR worklogAuthor = currentUser()) '
            , maxResults=False)

        # debug output
        if self.verbose:
            print(f"Found {len(issues)} issues\n")

        for issue in issues:
            # get issue detail because of worklogs
            issue_obj = self.jira.issue(issue.key)

            # debug output
            if self.verbose:
                print(f"\nIssue: {issue.key}")

            # get worklogs from issuees
            worklogs = issue_obj.fields.worklog.worklogs

            if self.verbose:
                print(f"Found {len(worklogs)} worklogs for current issue")

            if not worklogs == []:
                for worklog in worklogs:
                    # transform worklog to time entry
                    entries = entries + [self.transform_worklog(worklog, issue.key)]

        return self.modify_entries(entries)

    def transform_worklog(self, worklog, issue_key):

        tmp_entry = {}

        time_spent = worklog.timeSpent
        time_spent_seconds = worklog.timeSpentSeconds

        dt_worklog_started = parse(worklog.started)
        dt_worklog_ended = dt_worklog_started + relativedelta(seconds=time_spent_seconds)

        if self.verbose:
            print(f"Worklog start: {dt_worklog_started} spent: {time_spent}")

        tmp_entry['datetime_from'] = dt_worklog_started
        tmp_entry['datetime_to'] = dt_worklog_ended
        # TODO get description from tempo
        tmp_entry['description'] = worklog.comment
        tmp_entry['identificator'] = issue_key

        return tmp_entry

    def set_entries(self, entries=None):
        """
        Set entries

        Parameters
        ----------
        entries : list, required
            entries from self.get_entries()

        """
        # debug output
        if entries is None:
            entries = []
        if self.verbose:
            print("Set Entries Jira")

        # TODO pridat pravidla pro nastavovani
        # TODO pridat podporu vice jira spaces (magexo + drmax)

        for entry in entries:
            description = entry['description']
            identificator = entry['identificator']
            datetime_from = entry['datetime_from']

            if not identificator:
                # skip issue for delete
                if self.skip_set_entry("None"):
                    print(
                        f"Skipping worklog for set for issue without identificator with description {description}"
                    )
                    continue

            if self.skip_set_entry(identificator):
                print(
                    f"Skipping worklog for set for issue {identificator}"
                )
                continue

            # debug output
            if self.verbose:
                print(f"Proccessing time entry for issue {identificator}")

            time_spent_seconds = self.get_time_spent(entry)

            time_chunk_roundup_seconds = self.convert_to_seconds(self.time_chunk_roundup)
            if time_spent_seconds < time_chunk_roundup_seconds:
                # debug output
                if self.verbose:
                    print(f"Spent time in seconds {time_spent_seconds} is lower "
                          f"than roundup value in seconds {time_chunk_roundup_seconds}. "
                          f"Skipping record.")
                continue

            # round up
            time_spent_seconds = self.roundup_time_in_seconds(time_spent_seconds)

            # debug output
            if self.verbose:
                print(f"Adding worklog to issue \"{identificator}\": \n"
                      f"started={datetime_from}\n"
                      f"timeSpentSeconds={time_spent_seconds}\n"
                      f"comment={description}\n"
                      )

            if not self.debug:
                self.jira.add_worklog(identificator, started=datetime_from, timeSpentSeconds=time_spent_seconds,
                                      comment=description)
            else:
                print("Fake Adding worklog")

    def delete_entries(self, start_date=None, end_date=None):
        # TODO vyresit duplicitni kod s get_entries

        # debug output
        if self.verbose:
            print("Delete Entries Jira")

        end_date = end_date + datetime.timedelta(days=1)

        worklog_date_start = start_date.strftime("%Y-%m-%d")
        worklog_date_end = end_date.strftime("%Y-%m-%d")

        worklogs = self.tempo.get_worklogs(worklog_date_start, worklog_date_end, accountId=self.jira.current_user())

        for worklog in worklogs:

            # skip issue for delete
            if self.skip_delete_entry(worklog['issue']['key']):
                print(
                    f"Skipping worklog for delete for issue {worklog['issue']['key']} with id {worklog['jiraWorklogId']}"
                )
                continue

            worklog_jira = self.jira.worklog(worklog['issue']['key'], worklog['jiraWorklogId'])

            if self.verbose:
                print(f"Deleting worklog for issue {worklog['issue']['key']} with id {worklog['jiraWorklogId']}")

            if not self.debug:
                worklog_jira.delete()
            else:
                print("Fake Deleting worklog")

    def test(self, entries=None):
        print(type(self).__name__)
