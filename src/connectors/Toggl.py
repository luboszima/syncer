from toggl.TogglPy import Toggl as TogglClass
from datetime import timedelta
import json
from dateutil.parser import parse
from collections import defaultdict

from .Base import BaseSync
from exceptions import InstanceException, ParamException


# TODO pridat komentare
# TODO pridat debug pro nastaaveni zaznamu a mazani
# TODO pridat verbose hlasky

class TogglSync(BaseSync):
    user = False
    toggl = False
    projects = False

    def __init__(self,
                 username,
                 password,
                 token,
                 config,
                 debug,
                 verbose,
                 default_timezone,
                 time_chunk,
                 time_chunk_roundup
                 ):
        super(TogglSync, self).__init__(username=username,
                                        password=password,
                                        token=token,
                                        config=config,
                                        default_timezone=default_timezone,
                                        time_chunk=time_chunk,
                                        time_chunk_roundup=time_chunk_roundup,
                                        debug=debug,
                                        verbose=verbose
                                        )

        # init values
        self.user = self.get_user_info()
        self.projects = self.get_all_projects()
        self.config = json.loads(self.config)
        self.config_identificators = self.set_config_identificators()

    def login(self):
        self.toggl = TogglClass()

        if self.username and self.password:
            self.toggl.setAuthCredentials(self.username, self.password)

        if self.token:
            self.toggl.setAPIKey(self.token)

        if not self.toggl:
            raise InstanceException(f"${type(self).__name__} bad login.")

    def get_entry_identificator(self, entry):

        if not 'identificator-from' in self.config:
            self.config['identificator-from'] = 'description-prefix'

        if not 'identificator-separator' in self.config:
            self.config['identificator-separator'] = ':'

        identificator = ""
        identificator_from = self.config['identificator-from'].strip()
        identificator_separator = self.config['identificator-separator'].strip()

        if identificator_from == "project":
            identificator = entry['project']
            if not entry['project']:
                raise ParamException("Cannot find identifier for entry with description: " + entry['description'])

        elif identificator_from == "description-prefix":
            description_list = entry['description'].split(identificator_separator)

            if not description_list or len(description_list) == 1:
                raise ParamException("Cannot find identifier for entry with description: " + entry['description'])

            identificator = description_list[0]

        return identificator;

    def get_entry_description(self, entry):

        if not 'identificator-from' in self.config:
            self.config['identificator-from'] = 'description-prefix'

        if not 'identificator-separator' in self.config:
            self.config['identificator-separator'] = ':'

        description = ""
        identificator_from = self.config['identificator-from'].strip()
        identificator_separator = self.config['identificator-separator'].strip()

        if identificator_from == "project":
            description = entry['description']
        elif identificator_from == "description-prefix":
            description_list = entry['description'].split(identificator_separator)
            description_list.pop(0)
            description = identificator_separator.join(description_list).strip()

        if not description:
            raise ParamException("One of entries has an empty description")

        return description
    def get_entries(self, start_date="", end_date=""):
        # TODO udelat kontrolu jestli jes vse spravne nastaveno v toggl entries
        # - identifikator

        # debug output
        if self.verbose:
            print("Get Entries Toggl")

        raw_entries = self.get_entries_raw(start_date=start_date, end_date=end_date)

        # debug output
        if self.verbose:
            print("Find issues from date ", start_date.strftime("%Y-%m-%d"), " to date ",
                  end_date.strftime("%Y-%m-%d"))

        # debug output
        if self.verbose:
            print(f"Found {len(raw_entries)} time entries\n")

        entries = []

        for entry in raw_entries:
            tmp_entry = {}
            tmp_entry['datetime_from'] = parse(entry['start'])
            tmp_entry['datetime_to'] = parse(entry['end'])
            tmp_entry['description'] = self.get_entry_description(entry)
            tmp_entry['identificator'] = self.get_entry_identificator(entry)

            entries = entries + [tmp_entry]

        return self.modify_entries(entries)

    def set_entries(self, entries):
        print("Set Entries")
        # TODO

    def delete_entries(self, start_date=None, end_date=None):
        print("Delete Entries")
        # TODO

    def get_user_info(self):
        return self.toggl.request("https://api.track.toggl.com/api/v8/me")

    def get_user_workspaces(self):
        return self.user['data']['workspaces']

    def get_all_projects(self):
        all_projects = []
        for workspace in self.get_user_workspaces():
            projects = self.toggl.getWorkspaceProjects(workspace['id'])
            all_projects = all_projects + projects
        return all_projects

    def get_entries_from_workspace(self, start_date, end_date, from_workspace_id=""):
        user_id = self.user['data']['id']
        user_agent = "cloner"
        entries = []

        start_date_format = start_date.strftime("%Y-%m-%d")
        end_date_format = end_date.strftime("%Y-%m-%d")

        data = True
        page = 1

        while data:
            # Maximum date span (until - since) is one year
            response = self.toggl.request(
                f'https://api.track.toggl.com/reports/api/v2/details?workspace_id={from_workspace_id}&since={start_date_format}&page={page}&until={end_date_format}&user_agent={user_agent}&include_time_entry_ids=true&user_ids={user_id}')
            data = response['data']
            for time_entry in response['data']:
                entries.append(time_entry)
            page += 1

        return entries

    def merge_entries_by(self, attribute="project", entries=[]):
        results = {}
        without_attribute = []
        for entry in entries:
            if not entry[attribute]:
                without_attribute.append(entry)
            else:
                if entry[attribute] not in results.keys():
                    results[entry[attribute]] = [entry]
                else:
                    results[entry[attribute]].append(entry)

        return (results, without_attribute)

    def entries_duration(self, entries):
        project_duration = 0
        for entry in entries:
            duration = int(entry['dur'] / 1000)
            project_duration = project_duration + duration
        return project_duration

    def get_entries_raw(self, start_date="", end_date=""):
        all_entries = []
        for workspace in self.get_user_workspaces():
            entries = self.get_entries_from_workspace(start_date=start_date, end_date=end_date,
                                                      from_workspace_id=workspace['id'])
            all_entries = all_entries + entries

        return all_entries
