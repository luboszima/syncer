from datetime import date, datetime, timezone, timedelta
from dateutil import relativedelta


def get_class(kls):
    parts = kls.split('.')
    module = ".".join(parts[:-1])
    m = __import__(module)
    for comp in parts[1:]:
        m = getattr(m, comp)
    return m


# set start and end to morning and midnight
def adjust_days(start, end):
    return start.replace(hour=00, minute=00), end.replace(hour=23, minute=59)


def get_current_day():
    today = datetime.now().astimezone()
    return adjust_days(today, today)


def get_current_week():
    today = datetime.now().astimezone()
    entries_start_obj = today - timedelta(days=today.weekday())
    entries_end_obj = entries_start_obj + timedelta(days=6)

    return adjust_days(entries_start_obj, entries_end_obj)


def get_current_month():
    today = datetime.now().astimezone()
    nextmonth = today + relativedelta.relativedelta(months=1)
    this_month_last_day = nextmonth.replace(day=1) - timedelta(days=1)
    this_month_first_day = this_month_last_day.replace(day=1)

    return adjust_days(this_month_first_day, this_month_last_day)


def get_yesterday():
    today = datetime.now().astimezone()
    yesterday = today - timedelta(days=1)

    return adjust_days(yesterday, yesterday)


def get_last_week():
    today = datetime.now().astimezone()

    start_date = today + timedelta(-today.weekday(), weeks=-1)
    end_date = today + timedelta(-today.weekday() - 1)

    return adjust_days(start_date, end_date)


def get_last_month():
    today = datetime.now().astimezone()

    previous_month_last_day = today.replace(day=1) - timedelta(days=1)
    previous_month_first_day = previous_month_last_day.replace(day=1)

    return adjust_days(previous_month_first_day, previous_month_last_day)
