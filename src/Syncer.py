import argparse
import os
import json
import traceback
import sys
import calendar

from datetime import datetime, timezone, timedelta
from dateutil.parser import parse
from SyncerGeneral import Syncer
from libs import Helper

from connectors import *

def get_date_range(sync_this):
    entries_start = os.getenv('ENTRIES_START')
    entries_end = os.getenv('ENTRIES_END')
    entries_input_format = os.getenv('ENTRIES_INPUT_FORMAT')

    today = datetime.now().astimezone()

    if not entries_start:
        entries_start_obj = today.replace(hour=00, minute=00)
    else:
        entries_start_obj = datetime.strptime(entries_start, entries_input_format)

    if not entries_end:
        entries_end_obj = entries_start_obj.replace(hour=23, minute=59)
    else:
        entries_end_obj = datetime.strptime(entries_end, entries_input_format)

    if sync_this and not sync_this in ['day', 'week', 'month', 'yesterday', 'last-week', 'last-month']:
        raise ValueError("Parameter sync_this has a wrong value.")
    elif sync_this:
        if sync_this == 'day':
            entries_start_obj, entries_end_obj = Helper.get_current_day()
        elif sync_this == 'week':
            entries_start_obj, entries_end_obj = Helper.get_current_week()
        elif sync_this == 'month':
            entries_start_obj, entries_end_obj = Helper.get_current_month()
        elif sync_this == 'yesterday':
            entries_start_obj, entries_end_obj = Helper.get_yesterday()
        elif sync_this == 'last-week':
            entries_start_obj, entries_end_obj = Helper.get_last_week()
        elif sync_this == 'last-month':
            entries_start_obj, entries_end_obj = Helper.get_last_month()

    return entries_start_obj.strftime(entries_input_format), entries_end_obj.strftime(entries_input_format)

def config_arguments(parser):
    # TODO env variables z .env souboru maji vetsi prava nez inline parametry, opravit

    parser.add_argument('--from_connector', default=os.getenv('FROM_APP'))
    parser.add_argument('--from_username', default=os.getenv('FROM_USERNAME'))
    parser.add_argument('--from_password', default=os.getenv('FROM_PASSWORD'))
    parser.add_argument('--from_token', default=os.getenv('FROM_TOKEN'))
    parser.add_argument('--from_config', default=os.getenv('FROM_CONFIG'))
    parser.add_argument('--to_connector', default=os.getenv('TO_APP'))
    parser.add_argument('--to_username', default=os.getenv('TO_USERNAME'))
    parser.add_argument('--to_password', default=os.getenv('TO_PASSWORD'))
    parser.add_argument('--to_config', default=os.getenv('TO_CONFIG'))
    parser.add_argument('--to_token', default=os.getenv('TO_TOKEN'))
    parser.add_argument('--time_chunk', default=os.getenv('TIME_CHUNK'))
    parser.add_argument('--time_chunk_roundup', default=os.getenv('TIME_CHUNK_ROUNDUP'))
    parser.add_argument('--default_timezone', default=os.getenv('DEFAULT_TIMEZONE'))
    parser.add_argument('--debug', default=os.getenv('DEBUG'))
    parser.add_argument('--verbose', default=os.getenv('VERBOSE'))
    parser.add_argument('--entries_start', default=os.getenv('ENTRIES_START'))
    parser.add_argument('--entries_end', default=os.getenv('ENTRIES_END'))
    parser.add_argument('--entries_input_format', default=os.getenv('ENTRIES_INPUT_FORMAT'))
    parser.add_argument('--sync_this', default=os.getenv('SYNC_THIS'))

    return parser

# TODO vystupy s infem pro ktereho uzivatele se co syncuje
def main(
        from_connector="toggl",
        from_username="",
        from_password="",
        from_token="",
        from_config="",
        to_connector="jira",
        to_username="",
        to_password="",
        to_token="",
        to_config="",
        entries_start="",
        entries_end="",
        time_chunk="15m",
        time_chunk_roundup="5m",
        debug=False,
        verbose=False,
        default_timezone="Europe/Prague",
        entries_input_format="%Y-%m-%d"
):
    # get instance from
    from_connector_modified = from_connector.lower().capitalize()
    if verbose:
        print(f"# Trying create instance {from_connector_modified}")

    from_class = Helper.get_class(f'connectors.{from_connector_modified}Sync')
    from_instance = from_class(
        username=from_username,
        password=from_password,
        token=from_token,
        config=from_config,
        default_timezone=default_timezone,
        time_chunk=time_chunk,
        time_chunk_roundup=time_chunk_roundup,
        debug=debug,
        verbose=verbose
    )

    # get instance to
    to_connector_modified = to_connector.lower().capitalize()
    if verbose:
        print(f"# Trying create instance {to_connector_modified}")
    to_class = Helper.get_class(f'connectors.{to_connector_modified}Sync')
    to_instance = to_class(
        username=to_username,
        password=to_password,
        token=to_token,
        config=to_config,
        time_chunk=time_chunk,
        default_timezone=default_timezone,
        time_chunk_roundup=time_chunk_roundup,
        debug=debug,
        verbose=verbose
    )

    syncer = Syncer(
        from_instance=from_instance,
        to_instance=to_instance,
        start_date=datetime.strptime(entries_start, entries_input_format),
        end_date=datetime.strptime(entries_end, entries_input_format),
        time_chunk=time_chunk,
        time_chunk_roundup=time_chunk_roundup,
        default_timezone=default_timezone
    )
    syncer.sync()

if __name__ == '__main__':
    # TODO nastavit rezim podle ceho urcovat jmena issue, jestli projekt nebo tagy danych entries
    # TODO pridat podporu definovani vlastniho identifikatoru ukolu
    # --- projekt tag (done)
    # --- hashtag
    # --- prefix v description

    # TODO udelat alfa verzi a prvni tag 0.0.1 pro pouziti v magexo
    try:
        parser = argparse.ArgumentParser(description='Main parser')
        parser = config_arguments(parser)
        args = parser.parse_args()

        entries_start, entries_end = get_date_range(args.sync_this)

        args.debug = json.loads(args.debug)
        args.verbose = json.loads(args.verbose)

        if args.debug == True:
            print("Debug is on\n")
            print("FROM_APP: ", args.from_connector)
            print("FROM_USERNAME: ", args.from_username)
            print("FROM_PASSWORD: ", args.from_password)
            print("FROM_TOKEN: ", args.from_token)
            print("FROM_CONFIG: ", args.from_config)
            print("TO_APP: ", args.to_connector)
            print("TO_USERNAME: ", args.to_username)
            print("TO_PASSWORD: ", args.to_password)
            print("TO_TOKEN: ", args.to_token)
            print("TO_CONFIG: ", args.to_config)
            print("ENTRIES_START: ", entries_start)
            print("ENTRIES_END: ", entries_end)
            print("ENTRIES_INPUT_FORMAT: ", args.entries_input_format)
            print("TIME_CHUNK: ", args.time_chunk)
            print("TIME_CHUNK_ROUNDUP: ", args.time_chunk_roundup)
            print("DEFAULT_TIMEZONE: ", args.default_timezone)
            print("SYNC_THIS: ", args.sync_this, "\n")
            print("DEBUG: ", args.debug, "\n")

        main(
            # from
            from_connector=args.from_connector,
            from_username=args.from_username,
            from_password=args.from_password,
            from_token=args.from_token,
            from_config=args.from_config,
            # to
            to_connector=args.to_connector,
            to_username=args.to_username,
            to_password=args.to_password,
            to_token=args.to_token,
            to_config=args.to_config,
            # time
            entries_start=entries_start,
            entries_end=entries_end,
            time_chunk=args.time_chunk,
            time_chunk_roundup=args.time_chunk_roundup,
            default_timezone=args.default_timezone,
            # other
            debug=args.debug,
            verbose=args.verbose,
            entries_input_format=args.entries_input_format
        )
    except Exception as e:
        print(e, file=sys.stderr)
        traceback.print_exc()
