# Toggl Tracker to Jira Tempo synchronization

### Struktura

Classes:
- Syncer - hlavní logika syncování
- Toggl - práce s API Toggl
- Jira* - práce s api Jira

### Environment variables

- **DOCKER_REGISTRY** - docker registry odkud se stahuje docker image 
- **REPO_NAME** - cesta k image v docker registry
- **TIME_CHUNK** - casovy usek na ktery se budou zaokrouhlovat stopovane casy
- **TOGGL_ENTRIES_START** - cas kdy brat stopovane useky prace
- **TOGGL_ENTRIES_END** - cas do kdy brat stopovane useky prace
- **TOGGL_TOKEN** - token pro komunikaci s Toggl api
- **JIRA_USER** - uzivatel pro komunikaci s Jira api
- **JIRA_PASSWORD** - helso pro uzivatele pro komunikaci s Jira api 


Identifikator:
- project
- description-prefix
